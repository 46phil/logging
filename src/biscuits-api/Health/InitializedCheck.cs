﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace biscuits_api.Health
{
    public class InitializedCheck : IHealthCheck
    {
        private static DateTime StartupTime = DateTime.Now;

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var res = DateTime.Now > StartupTime.AddSeconds(1) ? HealthCheckResult.Healthy() : HealthCheckResult.Unhealthy();
            return Task.FromResult(res);
        }
    }
}
