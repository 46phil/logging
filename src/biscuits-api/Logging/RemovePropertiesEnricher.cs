﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace biscuits_api.Logging
{
    public class RemovePropertiesEnricher : ILogEventEnricher
    {
        private static readonly IReadOnlyList<string> RemoveProperties = new List<string>
        {
            "ActionId",
            "ConnectionId",
        };

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            foreach (var property in RemoveProperties)
            {
                logEvent.RemovePropertyIfPresent(property);
            }
        }
    }
}
