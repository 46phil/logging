﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biscuits_api
{
    public interface IBiscuitRepository
    {
        IReadOnlyList<Biscuit> Get();

        int Save(Biscuit biscuit);

        void Delete(int id);
    }
}
