﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biscuits_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BiscuitsController : ControllerBase
    {
        private readonly IBiscuitRepository biscuitRepository;

        public BiscuitsController(IBiscuitRepository biscuitRepository)
        {
            this.biscuitRepository = biscuitRepository;
        }

        [HttpGet]
        public IEnumerable<Biscuit> Get()
        {
            var trace = Request.Headers["traceparent"];

            Response.Headers.Add("traceparent", trace);

            return this.biscuitRepository.Get();
        }

        [HttpPost]
        public int Save([FromBody] Biscuit biscuit)
        {
            return this.biscuitRepository.Save(biscuit);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.biscuitRepository.Delete(id);
        }
    }
}
