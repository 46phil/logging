using biscuits_api.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OpenTelemetry;
using OpenTelemetry.Trace;
using Serilog;
using Serilog.Events;
using Serilog.Filters;
using Serilog.Formatting.Compact;
using Serilog.Templates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biscuits_api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            new LoggerConfiguration().CreateBootstrapLogger();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(
                    (context, config) =>
                    {
                        config.AddJsonFile("appsettings.json", false, true);
                        config.AddJsonFile("appsettings." + context.HostingEnvironment.EnvironmentName + ".json", true, true);
                        config.AddEnvironmentVariables();
                    })
                .UseSerilog((context, services, configuration) =>
                  configuration
                    .Filter.ByExcluding(
                         Matching.WithProperty<string>("RequestPath", v =>
                             "/health/startup".Equals(v, StringComparison.OrdinalIgnoreCase) ||
                             "/health/readiness".Equals(v, StringComparison.OrdinalIgnoreCase) ||
                             "/health/liveness".Equals(v, StringComparison.OrdinalIgnoreCase)))
                    .ReadFrom.Configuration(context.Configuration)
                    .Enrich.FromLogContext()
                    .Enrich.RemoveRedundantProperties()
                    .WriteTo.Console(new ExpressionTemplate("{ {t:@t, m:@m, level:@l, exception:@x, ..@p } }\n"))
                )
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
