﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace biscuits_api
{
    public class BiscuitRepository : IBiscuitRepository
    {
        private readonly ILogger<BiscuitRepository> logger;

        private readonly List<Biscuit> biscuits = new List<Biscuit>
            {
                new Biscuit(1, "Hobnob", "The marine of biscuits."),
                new Biscuit(2, "Ginger Nut", "A bit gingery."),
                new Biscuit(3, "Shortbread", "Makes you a bit thirsty."),
            };

        public BiscuitRepository(ILogger<BiscuitRepository> logger)
        {
            this.logger = logger;
        }

        public void Delete(int id)
        {
            this.logger.LogDebug($"About to delete {id}");

            var biscuit = this.biscuits.FirstOrDefault(b => b.Id == id);

            if(biscuit == null)
            {
                this.logger.LogError($"No biscuit found for id:{id}");
                throw new InvalidOperationException($"No biscuit found for id:{id}");
            }

            this.biscuits.Remove(biscuit);
            this.logger.LogInformation($"Deleted biscuit id:{id}");
        }

        public IReadOnlyList<Biscuit> Get()
        {
            this.logger.LogInformation("About to get, biscuits exist {data}", new { Count = this.biscuits.Count, Method = nameof(this.Get), Collection = nameof(this.biscuits) });
            this.logger.LogInformation("Retrieved all biscuits count {count}. {biscuits}", this.biscuits.Count, this.biscuits);
            return this.biscuits;
        }

        public int Save(Biscuit biscuit)
        {
            var index = this.biscuits.FindIndex(b => b.Id == biscuit.Id);

            if(index == -1)
            {
                biscuit = new Biscuit(this.biscuits.Count + 1, biscuit.Name, biscuit.Description);
                this.biscuits.Add(biscuit);
                this.logger.LogInformation($"Biscuit inserted, {biscuit}");
            }
            else
            {
                this.biscuits[index] = biscuit;
                this.logger.LogInformation($"Biscuit updated, {biscuit}");
            }

            return biscuit.Id;
        }
    }
}
