﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OpenTelemetry.Trace;
using Serilog;
using Serilog.Context;

namespace biscuits_api.Middleware
{
    public class LogContextMiddleware
    {
        private readonly RequestDelegate _next;

        private readonly string systemId = "BISKIT";
        private readonly string userId = "CPR0000";

        public LogContextMiddleware(RequestDelegate next)
        {
            this._next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext == null) throw new ArgumentNullException(nameof(httpContext));

            var traceId = Tracer.CurrentSpan.Context.TraceId.ToHexString();

            using (LogContext.PushProperty("SystemId", systemId))
            using (LogContext.PushProperty("UserId", userId))
            using (LogContext.PushProperty("TraceId", traceId.ToString()))
            {
                await this._next(httpContext);
            }
        }
    }
}
