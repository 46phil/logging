using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using OpenTelemetry;
using OpenTelemetry.Exporter;
using OpenTelemetry.Trace;
using OpenTelemetry.Resources;
using OpenTelemetry.Instrumentation.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace biscuits_api
{
    public class Startup
    {
        private static readonly IReadOnlyList<string> IgnoreRequestPaths = new List<string>
            {
               "/health/startup",
               "/health/readiness",
               "/health/liveness",
            };

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2Support", true);

            //services.Configure<AspNetCoreInstrumentationOptions>(options =>
            //{
            //    options.Filter = (httpContext) =>
            //    {
            //        return !IgnoreRequestPaths.Any(p => p.Equals(httpContext.Request.Path, StringComparison.InvariantCultureIgnoreCase));
            //    };
            //});

            services.AddOpenTelemetryTracing(
                builder =>
                {
                    builder
                        .AddAspNetCoreInstrumentation()
                        .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService("biscuits-api"))
                        .AddOtlpExporter(options =>
                        {
                            options.Endpoint = new Uri(this.Configuration["Trace_Collector_Url"]);
                        });
                });

            services.AddHealthChecks()
                .AddCheck<Health.InitializedCheck>("Initialized Checkup");

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "biscuits_api", Version = "v1" });
            });

            services.AddSingleton<IBiscuitRepository, BiscuitRepository>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseMiddleware<Middleware.LogContextMiddleware>();
            loggerFactory.AddSerilog();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "biscuits_api v1"));
            }

            app.UseRouting();

            app.UseSerilogRequestLogging();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health/startup");
                endpoints.MapHealthChecks("/health/readiness", new HealthCheckOptions { Predicate = _ => false });
                endpoints.MapHealthChecks("/health/liveness", new HealthCheckOptions { Predicate = _ => false });
                endpoints.MapControllers();
            });
        }
    }
}
