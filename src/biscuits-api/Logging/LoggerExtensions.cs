﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace biscuits_api.Logging
{
    public static class LoggerExtensions
    {
        public static LoggerConfiguration WithCaller(this LoggerEnrichmentConfiguration loggerEnrichmentConfiguration)
        {
            return loggerEnrichmentConfiguration.With<CallerEnricher>();
        }


        public static LoggerConfiguration RemoveRedundantProperties(this LoggerEnrichmentConfiguration loggerEnrichmentConfiguration)
        {
            return loggerEnrichmentConfiguration.With<RemovePropertiesEnricher>();
        }
    }
}
