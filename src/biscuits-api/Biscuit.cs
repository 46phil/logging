﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace biscuits_api
{
    public class Biscuit
    {
        public Biscuit()
        {

        }

        public Biscuit(int id, string name, string description)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public override string ToString()
        {
            return $"id:{this.Id}, name:{this.Name}";
        }
    }
}
