param (
	[parameter(Mandatory = $true)][string] $chart,
	[parameter(Mandatory = $true)][string] $version
)

$repository = "calromphil/$chart"
$namespace = "dev"
$release = "$namespace-$chart"
Set-Location "../src"

$name = "${repository}:${version}"

Write-Host $name

docker build -f biscuits-api/Dockerfile -t $name .
docker push $name 

Set-Location "../deploy"

helm upgrade --install --set namespace=$repository,image.tag=$version -n $namespace $release $chart